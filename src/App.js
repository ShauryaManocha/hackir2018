import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Home from "./Components/Home"
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import { TwitterTimelineEmbed, TwitterShareButton, TwitterFollowButton, TwitterHashtagButton, TwitterMentionButton, TwitterTweetEmbed, TwitterMomentShare, TwitterDMButton, TwitterVideoEmbed, TwitterOnAirButton } from 'react-twitter-embed';

class App extends Component {
  constructor(){
    super()
    this.state = {
      home:<Home setPage = {this.setPage()} school="Craig Kielburger"/>,
      currentPage : {}
    }
    this.setPage("home");
    
  }

  setPage = (page)=>{
    var newState = this.state
    switch(page){
      case "home":
        newState.currentPage = this.state.home
      
      break;

    }
    this.setState(newState)
  }
  render(){
    return (
      <div>
        {this.state.currentPage}
      </div>
    );
  }
}

export default App;
