import React, { Component } from 'react'
import firebase from './firebase'
import {Text} from "grommet"

var db = firebase.firestore();
db.settings({
    timestampsInSnapshots: true
  });





export default class GetAnnouncements extends Component {
    constructor(props){
        super(props)
        this.state = {
            announcements:[],
            school:this.props.school,
        }
        this.getAnnouncements()
    }
    getAnnouncements = async()=>{
    
        console.log(this.state)
       
        return await db.collection("Schools").doc(this.state.school).collection("Announcements").get().then(snapshot=>{
            snapshot.forEach(async doc=>{
                var announcements = this.state.announcements
                var prevState = this.state
                var title = await doc.data().Title
                var data = await doc.data().Data
                var currentText = <Text>{title}: {data}</Text>
                announcements.push(currentText)
                announcements.push(<br/>)
                prevState.announcements = announcements
                this.setState(prevState)
                console.log(doc.data())
            })
        })
       
    }
  render() {
     
    return (
      <div>
        {this.state.announcements}
      </div>
    )
  }
}
